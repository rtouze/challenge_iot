package hackathon.iot

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

import java.text.SimpleDateFormat
import java.util.{Locale, TimeZone}
import java.util.Calendar
import java.text.SimpleDateFormat

import java.util.UUID;

import java.io._
import org.apache.commons._

class Injections extends Simulation {

	var numberOfMsgs = 10000
	
	val httpProtocol = http.baseURL("http://192.168.1.1/").inferHtmlResources()
    val header = Map(
		"Accept" -> "application/json; charset=UTF-8 ",
		"Upgrade-Insecure-Requests" -> "1")
	
	//this is the adress of the http post you should put your local server
    var url="http://192.168.1.1/messages"
	
	//the Date formatter who makes the date on the DateTime RFC3339
	val formatter  = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSXXX")
		formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		
	//unique Id generator for each message sent
	def generateId():String={
		UUID.randomUUID().toString()+UUID.randomUUID().toString()
	}
    
    //generate random numbers and keep the min the max and the average 
    def generateNum(sensorTypeIndex:Int):Int={
		val r = scala.util.Random
		var value = r.nextInt
		return value
	}
	
	//scneario 1 
	val scn1 = scenario("injecteur 1").repeat(numberOfMsgs){ 
				exec(http("request_1")
					.post(url)
					.body(StringBody(session=>"""{"id":""""+generateId()+"""",
						                      "timestamp":""""+formatter.format(Calendar.getInstance().getTime())+"""",
								      "sensorType":"1",
								      "value":""""+generateNum(0)+"""" }""")).asJSON
					.headers(header)
					.check(status.is(200))
					)		
	}

	//scenario 2
	val scn2 = scenario("injecteur 2").repeat(numberOfMsgs){  
				exec(http("request_2")
					.post(url)
					.body(StringBody(session=>"""{"id":""""+generateId()+"""",
								      "timestamp":""""+formatter.format(Calendar.getInstance().getTime())+"""",
								      "sensorType":"2",
								      "value":""""+generateNum(1)+"""" }""")).asJSON
					.headers(header)
					.check(status.is(200))
					)		
	}

	//scenario 3
	val scn3 = scenario("injecteur 3").repeat(numberOfMsgs){  
				exec(http("request_3")
					.post(url)
					.body(StringBody(session=>"""{"id":""""+generateId()+"""",
								      "timestamp":""""+formatter.format(Calendar.getInstance().getTime())+"""",
								      "sensorType":"3",
								      "value":""""+generateNum(2)+"""" }""")).asJSON
					.headers(header)
					.check(status.is(200))
					)		
	}
	

	//scenario 4

	val scn4 = scenario("injecteur 4").repeat(numberOfMsgs){ 	    
				exec(http("request_4")
					.post(url)
					.body(StringBody(session=>"""{"id":""""+generateId()+"""",
								      "timestamp":""""+formatter.format(Calendar.getInstance().getTime())+"""",
								      "sensorType":"4",
								      "value":""""+generateNum(3)+"""" }""")).asJSON
					.headers(header)
					.check(status.is(200))
					)		
	}

	//scenario 5

	val scn5 = scenario("injecteur 5").repeat(numberOfMsgs){ 
				exec(http("request_5")
					.post(url)
					.body(StringBody(session=>"""{"id":""""+generateId()+"""",
								      "timestamp":""""+formatter.format(Calendar.getInstance().getTime())+"""",
								      "sensorType":"5",
								      "value":""""+generateNum(4)+"""" }""")).asJSON
					.headers(header)
					.check(status.is(200))
					)		
	}

	//scenario 6
	val scn6 = scenario("injecteur 6").repeat(numberOfMsgs){ 
				exec(http("request_6")
					.post(url)
					.body(StringBody(session=>"""{"id":""""+generateId()+"""",
								      "timestamp":""""+formatter.format(Calendar.getInstance().getTime())+"""",
								      "sensorType":"6",
								      "value":""""+generateNum(5)+"""" }""")).asJSON
					.headers(header)
					.check(status.is(200))
					)		
	}

	//scenario 7
	val scn7 = scenario("injecteur 7").repeat(numberOfMsgs){ 
				exec(http("request_7")
					.post(url)
					.body(StringBody(session=>"""{"id":""""+generateId()+"""",
								      "timestamp":""""+formatter.format(Calendar.getInstance().getTime())+"""",
								      "sensorType":"7",
								      "value":""""+generateNum(6)+"""" }""")).asJSON
					.headers(header)
					.check(status.is(200))
					)		
	}

	//scenario 8
	val scn8 = scenario("injecteur 8").repeat(numberOfMsgs){
				exec(http("request_8")
					.post(url)
					.body(StringBody(session=>"""{"id":""""+generateId()+"""",
								      "timestamp":""""+formatter.format(Calendar.getInstance().getTime())+"""",
								      "sensorType":"8",
								      "value":""""+generateNum(7)+"""" }""")).asJSON
					.headers(header)
					.check(status.is(200))
					)		
	}

	//scenario 9

	val scn9 = scenario("injecteur 9").repeat(numberOfMsgs){ 
				exec(http("request_9")
					.post(url)
					.body(StringBody(session=>"""{"id":""""+generateId()+"""",
								      "timestamp":""""+formatter.format(Calendar.getInstance().getTime())+"""",
								      "sensorType":"9",
								      "value":""""+generateNum(8)+"""" }""")).asJSON
					.headers(header)
					.check(status.is(200))
					)		
	}

	//scenario 10
	val scn10 = scenario("injecteur 10").repeat(numberOfMsgs){ 
				exec(http("request_10")
					.post(url)
					.body(StringBody(session=>"""{"id":""""+generateId()+"""",
								      "timestamp":""""+formatter.format(Calendar.getInstance().getTime())+"""",
								      "sensorType":"10",
								      "value":""""+generateNum(9)+"""" }""")).asJSON
					.headers(header)
					.check(status.is(200))
					)								
     }

	/**we run the scenarios and assert that 100% 
  	*of messages were received
  	*/
	  setUp(scn1.inject(atOnceUsers(1)),
	  	scn2.inject(atOnceUsers(1)),
	  	scn3.inject(atOnceUsers(1)),
	  	scn4.inject(atOnceUsers(1)),
	  	scn5.inject(atOnceUsers(1)),
	  	scn6.inject(atOnceUsers(1)),
	  	scn7.inject(atOnceUsers(1)),
	  	scn8.inject(atOnceUsers(1)),
	  	scn9.inject(atOnceUsers(1)),
	  	scn10.inject(atOnceUsers(1)))
	  .protocols(httpProtocol)
}
