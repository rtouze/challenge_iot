Ceci est notre repository pour le hackathon IoT interne tenu en mai 2016

Les différents tests couvrent plusieurs technologies :

* Python + Redis
* NodeJS + Redis (branche node\_redis)
* NodeJS + Postgresql (branche node\_pg)
* Elixir + Redis

Au final, la solution node + postgresql a été choisie pour la finale du Hackathon.

# Description des solutions envisagées

## Python + Redis

L'implémentation Python utilise le framework Flask et le serveur WSGI gevent.
[Redis](http://redis.io/) est utilisé pour stocker les messages sous forme de
liste.  

C'était plutôt sympa pour faire un test de déploiement Python + Gevent, mais le
serveur manquait de vélocité déjà sur PC. Au final, cela semble assez difficile
de faire de la perf avec du Python (pas infaisable, difficile).

## Elixir + Redis

[Elixir][el] tourne sur Erlang, ce qui semblait être un choix intéressant. Les
premiers tests ont montré un niveau de performance très correct mais en dessous de
la solution Node + Redis. Nous avons utilisé le framework [Phoenix][phoe].  

Au final, comme nous ne maîtrisions pas suffisamment le langage, nous avons
préféré nous concentrer sur Node.

## Node

Nous avons commencé à utiliser Node pour le serveur et Redis pour le stockage
des messages. Nous avons utilisé [cluster server][cs] pour présenter répartir la
charge sur 4 processus node. Cette solution était plutôt performante. Nous
avons utilisé le module *http* présent en built in sur Node.  

Malheureusement, nous avons rapidement rencontré des problèmes avec le calcul
de la synthèse. Les itérations sur le script Gatling ne nous ont pas aidés pour
déboguer notre application. Au final nous nous orientions sur l'utilisation
d'un *sorted set* Redis pour récupérer les valeurs sur une plage définie.

Nous avons développé une solution de repli basée sur PostgreSQL, ce qui nous
permettait d'avoir une synthèse correcte en une requête.

Au final, nous avons présenté la version Node + PostgreSQL après une nuit
d'optimisation pour éviter les écritures trop fréquentes sur disque. Ça reste
lent (plus de 17min pour 1 000 000 messages et un plantage pour 10 000 000)

Nous n'avons pas fait d'optimisation sur la couche TCP (genre keep-alive),
c'est un tort.  

D'après nous, le problème sur la version Node + Redis était sur la
représentation des grands nombres, qui rendait le calcul des synthèses erroné.
Nous aurions dû rechercher un module pour gérer les bigInteger. Nous avons manqué
de temps.

Nous avons passé un temps hallucinant sur le formatage de la réponse, pourtant
en JSON, surtout pour renvoyer un décimal avec une précision sur 2 au
centième ! On aurait peut-être dû faire du Java, comme les copains…

[el]: http://elixir-lang.org/ "Elixir"
[phoe]: http://www.phoenixframework.org/ "Framework Phoenix"
[cs]: https://www.npmjs.com/package/cluster-server "Cluster Server"
