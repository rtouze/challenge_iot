process.env.NODE_ENV = 'production'

var http = require('http')
var url = require('url')
var moment = require('moment')
var redis = require('redis')
var client = redis.createClient({detect_buffers: true})

http.ServerResponse.prototype.send = function (code, body) {
  this.writeHead(code)
  this.end(body)
}

function handlePostMessage (req, res) {
  var fullBody = ''

  req.on('data', function (chunk) {
    fullBody += chunk.toString()
  })

  req.on('end', function () {
    var message

    try {
      message = JSON.parse(fullBody)
    } catch (error) {
      res.send(500)
    }

    var timestampSecond = moment(message.timestamp).format('X')
    var args = ['messages', timestampSecond, fullBody]
    client.zadd(args, function (err, reply) {
      if (err) res.send(500)
      res.send(200)
    })
  })
}

function handleGetSynthesis (req, res, queryObject) {
  var duration = queryObject.duration
  var timestamp = queryObject.timestamp
  var start = moment(timestamp).format('X')
  var end = start + duration

  client.zrangebyscore(new Buffer('messages'), start, end, function (err, reply) {
    if (err) res.send(500)

    var replyJson = []
    if (Object.prototype.toString.call(reply) === '[object Array]') {
      replyJson = reply.map(function (buf) {
        return JSON.parse(buf)
      })
    }

    var sensorTypeArray = {}
    replyJson.forEach(function (o) {
      if (!sensorTypeArray[o.sensorType]) {
        sensorTypeArray[o.sensorType] = {minValue: Number(o.value), maxValue: Number(o.value), count: 1, sum: Number(o.value)}
      } else {
        if (Number(o.value) < sensorTypeArray[o.sensorType].minValue) {
          sensorTypeArray[o.sensorType].minValue = Number(o.value)
        }

        if (Number(o.value) > sensorTypeArray[o.sensorType].maxValue) {
          sensorTypeArray[o.sensorType].maxValue = Number(o.value)
        }

        sensorTypeArray[o.sensorType].count++
        sensorTypeArray[o.sensorType].sum += Number(o.value)
      }
    })

    var result = []
    for (var sensorType in sensorTypeArray) {
      result.push({
        sensorType: Number(sensorType),
        minValue: sensorTypeArray[sensorType].minValue,
        maxValue: sensorTypeArray[sensorType].maxValue,
        // sumValue: sensorTypeArray[sensorType].sum,
        // countValue: sensorTypeArray[sensorType].count,
        // mediumValue: Number((sensorTypeArray[sensorType].sum / sensorTypeArray[sensorType].count).toFixed(2))
        // mediumValue: +Number(sensorTypeArray[sensorType].sum / sensorTypeArray[sensorType].count).toFixed(2)
        mediumValue: Math.round(sensorTypeArray[sensorType].sum / sensorTypeArray[sensorType].count * 100) / 100
      })
    }

    res.send(200, JSON.stringify(result))
  })
}

function handleRequest (req, res) {
  req.on('error', function (err) {
    res.send(400, err)
  })

  var urlParsed = url.parse(req.url, true)
  var pathname = urlParsed.pathname

  if (req.method === 'POST' && pathname === '/messages') {
    handlePostMessage(req, res)
  } else if (req.method === 'GET' && pathname === '/messages/synthesis') {
    handleGetSynthesis(req, res, urlParsed.query)
  } else {
    console.log('404 url: ' + req.url)
    res.send(404)
  }
}

http.createServer(handleRequest).listen(8080)
