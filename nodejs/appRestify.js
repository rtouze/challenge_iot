process.env.NODE_ENV = 'production'

var restify = require('restify')

function respondPost(req, res, next) {
  res.send(200)
  next()
}

var server = restify.createServer()

server.post('/messages', respondPost)

server.listen(8080, function() {
  console.log('%s listening at %s', server.name, server.url)
})