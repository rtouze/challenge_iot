process.env.NODE_ENV = 'production'

var express = require('express')
var bodyParser = require('body-parser')
var redis = require('redis')
var client = redis.createClient()

var app = express()
app.use(bodyParser.json()) // for parsing application/json

app.post('/messages', function (req, res) {
  res.status(200).send(null)
  client.set('message:' + req.body.id, JSON.stringify(req.body), function (err, reply) {
    if (err) res.status(500).json({ error: err })
    res.status(200).send(null)
  })
})

app.get('/messages/synthesis', function (req, res) {
  res.send('Service fournissant une synthèse des données sur les 60 dernières minutes, minute en cours incluse.')
})

app.listen(8080, function () {
  console.log('Example app listening on port 8080!')
})