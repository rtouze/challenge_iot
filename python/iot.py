#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Template for Vim"""

from collections import OrderedDict
import redis
import json

VALUE_KEY = 'v'
TYPE_KEY = 's'
TIMESTAMP_KEY = 't'
ID_KEY = 'i'

class RedisRepo:
    def __init__(self):
        self._red = redis.StrictRedis(host='localhost', port=6379, db=0)

    def save(self, sensor_data):
        self._red.rpush('message', json.dumps(sensor_data.to_dict()))

    def clean(self):
        self._red.delete('message')

    def retrieve_all(self):
        return self._red.lrange('message', 0, -1)


class SensorData:
    def __init__(self, json_data):
        self._data = json.loads(json_data)
        self.sid = self._data['id']
        self.timestamp = self._data['timestamp']
        self.type = self._data['sensorType']
        self.value = self._data['value']

    def save(self):
        messages.append(self)
        return self

    def to_dict(self):
        return {
            ID_KEY: self.sid,
            TIMESTAMP_KEY: self.timestamp,
            TYPE_KEY: self.type,
            VALUE_KEY: self.value
        }

    def __str__(self):
        return '{0} - {1}'.format(self.sid, self.value)


class MessageDict(OrderedDict):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def update_min_max(self, sensor_type, sensor_value):
        val = self.setdefault(
            sensor_type,
            {'minValue':sensor_value, 'maxValue':sensor_value}
        )
        if sensor_value < val['minValue']:
            val['minValue'] = sensor_value
        if sensor_value > val['maxValue']:
            val['maxValue'] = sensor_value

    @property
    def synthesis(self):
        return [
            {
                'sensorType': k,
                'minValue': self[k]['minValue'],
                'maxValue': self[k]['maxValue'],
                'mediumValue': get_medium_value(self[k]['minValue'], self[k]['maxValue'])
            } for k in self
        ]


def agreg_from_json_to_json(stored_messages):
    res = MessageDict()
    for m in [json.loads(js.decode('utf8')) for js in stored_messages]:
        res.update_min_max(m[TYPE_KEY], m[VALUE_KEY])
    return res.synthesis 


def get_medium_value(a, b):
    return int(a + ((b-a)//2))
