#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests

from datetime import datetime
import json
import urllib
import gevent, gevent.pool
import uuid

URL = 'localhost:4000'
PROXIES = None # {'http': 'http://localhost:8000' }

def post_data(data):
    """Function comments"""
    if PROXIES:
        requests.post('http://{0}/messages'.format(URL), json=data, proxies=PROXIES)
    else:
        requests.post('http://{0}/messages'.format(URL), json=data)

def get_data():
    """Function comments"""
    requests.get('http://{0}/messages/synthesis'.format(URL))

def process(i):
    """Function comments"""
    data={
        'id': str(uuid.uuid4()),
        'timestamp': datetime.today().isoformat(),
        'sensorType': i%5,
        'value': i*3245
    }
    post_data(data)
    if i%20 == 0:
        print('{} request posted'.format(i))
        get_data()
    

if __name__ == '__main__':
    pool = gevent.pool.Pool(20)
    for i in range(500):
        pool.spawn(process, i)
    pool.join(2)
