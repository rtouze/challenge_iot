#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Template for Vim"""

from iot import agreg_from_json_to_json, SensorData
import json


def test_agreg_from_json_data(): 
    data = [
        json.dumps(SensorData(json.dumps(d)).to_dict())
        for d in [
            {'id': 'test_sensor', 'timestamp': '', 'sensorType': 1, 'value': 1},
            {'id': 'test_sensor', 'timestamp': '', 'sensorType': 1, 'value': 2},
            {'id': 'test_sensor', 'timestamp': '', 'sensorType': 1, 'value': 3},
            {'id': 'test_sensor', 'timestamp': '', 'sensorType': 2, 'value': 10},
            {'id': 'test_sensor', 'timestamp': '', 'sensorType': 2, 'value': 20},
        ]
    ]
    result = agreg_from_json_to_json(data)
    assert result[1]['sensorType'] == 2
    assert result[1]['minValue'] == 10
    assert result[1]['maxValue'] == 20
    assert result[1]['mediumValue'] == 15
