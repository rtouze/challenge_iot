#!/usr/bin/env python
# -*- coding: utf-8 -*-

from gevent.wsgi import WSGIServer
from iot_challenge import app
import redis

red = redis.StrictRedis(host='localhost', port=6379, db=0)
red.delete('message')
http_server = WSGIServer(('', 5000), app)
http_server.serve_forever()
