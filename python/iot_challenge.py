#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Template for Vim"""

__author__ = 'romain.touze@capgemini.com'

from flask import Flask, request, Response
import json
from collections import OrderedDict
import redis
from iot import RedisRepo, SensorData, agreg_from_json_to_json


app = Flask(__name__)
repo = RedisRepo()


@app.route('/messages', methods=['POST'])
def post_message():
    data = request.data
    app.logger.debug('data ' + data.decode('utf8'))
    app.logger.debug(data)
    repo.save(SensorData(data.decode('utf8')))  
    return Response(status=200)


@app.route('/messages/synthesis', methods=['GET'])
def get_message_synthesis():
    return json.dumps(agreg_from_json_to_json(repo.retrieve_all()))


if __name__ == '__main__':
    app.run(debug=True)
