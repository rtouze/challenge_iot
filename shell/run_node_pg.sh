#!/usr/bin/env bash

export PATH=/opt/pgsql/bin:$PATH 
export LD_LIBRARY_PATH=/opt/pgsql/lib:$LD_LIBRARY_PATH 

echo "run node and pg"

/opt/pgsql/bin/pg_ctl -D /opt/projects/data start > /dev/null 2>&1 &
sleep 1
echo "truncate"
/opt/pgsql/bin/psql -U iot -d iot -c "truncate table message;"
cd /opt/projects/node_pg/nodejs
sudo npm run startHttp
cd -
/opt/pgsql/bin/pg_ctl -D /opt/projects/data stop > /dev/null 2>&1 &
