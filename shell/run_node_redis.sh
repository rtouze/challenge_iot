#!/usr/bin/env bash
echo "run node and redis"

# redis
cd /opt/projects
rm dump.rdb
/opt/bin/redis-server /opt/projects/redis.conf > /dev/null 2>&1 &
cd /opt/projects/node_redis/nodejs/
sudo npm run startHttp
cd -
