defmodule IotEx.MessageController do
  use IotEx.Web, :controller

  def messages(conn, _params) do
    #{:ok, red_conn} = Redix.start_link("redis://localhost:6379/0", name: :redix)
    #Redix.command(red_conn, ~w(RPUSH message ))
    send_resp(conn, 200, "")
  end

  def synthesis(conn, _params) do
    send_resp(conn, 200, "coucou")
  end
end
