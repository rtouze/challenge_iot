defmodule IotEx.Router do
  use IotEx.Web, :router

  pipeline :api do
    plug :accepts, ["json", "application/json"]
  end

  scope "/messages", IotEx do
    pipe_through :api
    post "/", MessageController, :messages
  end

  scope "/messages/synthesis", IotEx do
    pipe_through :api
    get "/", MessageController, :synthesis
  end

end
