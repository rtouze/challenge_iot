ExUnit.start

Mix.Task.run "ecto.create", ~w(-r IotEx.Repo --quiet)
Mix.Task.run "ecto.migrate", ~w(-r IotEx.Repo --quiet)
Ecto.Adapters.SQL.begin_test_transaction(IotEx.Repo)

